import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class asg {
    private JPanel root;
    private JButton spa;
    private JButton zoni;
    private JButton shirono;
    private JButton anmitsu;
    private JButton mochi;
    private JTextPane orderitem;
    private JTextPane sum;
    private JButton OK;
    private JButton Udon;
    private JButton coffee;
    private JButton houji;
    private JButton milk;
    private JTextPane total;
    int totalyen;
    double in;
    double out;


    void order(String food, int yen){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " +
                        food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Order for "+ food +" received.");
            String currentText=orderitem.getText();

            orderitem.setText(currentText+food+ "   " + yen + " yen\n");

            totalyen+= yen;
            in =totalyen*1.1;
            out=totalyen*1.08;

            total.setText("Total: "+totalyen +"yen+tax\n"
            +"eat in:"+ (int)in +" yen\n"
                    +"take away: "+(int)out+" yen\n");

        }
    }


    public asg() {
        zoni.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Zosui" , 730);
            }
        });

        spa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spaghetti",820);

            }
        });


        anmitsu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cream anmitsu",790);
            }
        });

        mochi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gohei-mochi",570);
            }
        });

        coffee.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee", 500);
            }
        });

        milk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Strawberry milk", 650);

            }
        });

        OK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){

                    in=totalyen*1.1;
                    out=totalyen*1.08;

                    JOptionPane.showMessageDialog(null,
                            "Thank you!\n"+
                           "eat in:"+ (int)in +" yen\n"
                                    +"take away: "+(int)out+" yen\n");

                    totalyen=0;

                    orderitem.setText("");
                    total.setText("total: 0 yen");

                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("asg");
        frame.setContentPane(new asg().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
